##
##  pira32ctl
##
##  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
##
##  This file is part of pira32ctl.
##
##  pira32ctl is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  any later version.
##
##  pira32ctl is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with pira32ctl. If not, see <http://www.gnu.org/licenses/>.
##

curdir:= $(shell pwd)
ifndef GOROOT
GOCMD := GOPATH=$(curdir) go
else
GOCMD := GOPATH=$(curdir) $(GOROOT)/bin/go
endif

EXECUTEABLE := pira32ctl

LIBS := "code.helsinki.at/goserial" \
        "github.com/gorilla/websocket" \
        "github.com/spreadspace/telgo"

.PHONY: getlibs updatelibs vet format build clean distclean
all: build


getlibs:
	@$(foreach lib,$(LIBS), echo "fetching lib: $(lib)"; $(GOCMD) get $(lib);)

updatelibs:
	@$(foreach lib,$(LIBS), echo "updating lib: $(lib)"; $(GOCMD) get -u $(lib);)

getlibs-noinstall:
	@$(foreach lib,$(LIBS), echo "fetching lib: $(lib)"; $(GOCMD) get -d $(lib);)

vet:
	@echo "vetting: $(EXECUTEABLE)"
	@$(GOCMD) vet $(EXECUTEABLE)

format:
	@echo "formating: $(EXECUTEABLE)"
	@$(GOCMD) fmt $(EXECUTEABLE)

test:
	@echo "testing: $(EXECUTEABLE)"
	@$(GOCMD) test $(EXECUTEABLE)

build: getlibs
	@echo "installing: $(EXECUTEABLE)"
	@$(GOCMD) install $(EXECUTEABLE)

build-alix: getlibs-noinstall
	@echo "cross-building: $(EXECUTEABLE)"
	@mkdir -p bin/linux_386
	@GOARCH=386 GO386=387 CGO_ENABLED=0 $(GOCMD) build -o bin/linux_386/$(EXECUTEABLE) $(EXECUTEABLE)

clean:
	rm -rf pkg/*/$(EXECUTEABLE)
	rm -rf bin

distclean: clean
	@$(foreach dir,$(shell ls src/),$(if $(subst $(EXECUTEABLE),,$(dir)),$(shell rm -rf src/$(dir))))
	rm -rf pkg
