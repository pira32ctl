//
//  pira32ctl
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of pira32ctl.
//
//  pira32ctl is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  pira32ctl is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with pira32ctl. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

var (
	p32l  = log.New(os.Stderr, "[pira32ctl]\t", log.LstdFlags)
	p32dl = log.New(ioutil.Discard, "[pira32ctl-dbg]\t", log.LstdFlags)
)

func init() {
	if _, exists := os.LookupEnv("PIRA32CTL_DEBUG"); exists {
		p32dl.SetOutput(os.Stderr)
	}
}

type envStringValue string

func newEnvStringValue(key, dflt string) *envStringValue {
	if envval, exists := os.LookupEnv(key); exists {
		return (*envStringValue)(&envval)
	} else {
		return (*envStringValue)(&dflt)
	}
}

func (s *envStringValue) Set(val string) error {
	*s = envStringValue(val)
	return nil
}

func (s *envStringValue) Get() interface{} { return string(*s) }

func (s *envStringValue) String() string { return fmt.Sprintf("%s", *s) }

func main() {
	help := flag.Bool("help", false, "show usage")

	flag.Parse()
	if *help {
		flag.Usage()
		return
	}

	p32l.Printf("just started...")
	p32l.Printf("shuttind down")
}
