//
//  pira32ctl
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of pira32ctl.
//
//  pira32ctl is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  pira32ctl is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with pira32ctl. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"errors"
	"fmt"
	"time"

	"code.helsinki.at/goserial"
)

const (
	REQ_PERSISTANT = "*"

	RESP_OK          = "+"
	RESP_UNKNOWN_CMD = "!"
	RESP_INVALID_ARG = "-"
	RESP_PARTIAL     = "/"

	storeExecutionTime = 10 * time.Millisecond

	respTimeout = 500 * time.Millisecond
)

var (
	ErrInvalidResp = errors.New("got invalid response")
	ErrTimeout     = errors.New("timeout")
	ErrUnknownCmd  = errors.New("unknown command")
	ErrInvalidArg  = errors.New("invalid argument")
	ErrPartial     = errors.New("partial success")
)

type Command interface {
	AllowedMethods() (get, assign, store, assignstore bool)
	Get(port *goserial.Port, params ...interface{}) (interface{}, error)
	Assign(port *goserial.Port, params ...interface{}) error
	Store(port *goserial.Port, params ...interface{}) error
	AssignStore(port *goserial.Port, params ...interface{}) error
}

type BasicCommand struct {
	code        string
	description string
	exec_time   time.Duration
	get         bool
	assign      bool
	store       bool
	assignStore bool
}

func (c BasicCommand) AllowedMethods() (bool, bool, bool, bool) {
	return c.get, c.assign, c.store, c.assignStore
}

func (c BasicCommand) Get(port *goserial.Port, params ...interface{}) (interface{}, error) {
	if len(params) != 0 {
		return nil, fmt.Errorf("this command takes no arguments")
	}
	// TODO: implement this
	return nil, nil
}

func (c BasicCommand) Assign(port *goserial.Port, params ...interface{}) error {
	if len(params) != 1 {
		return fmt.Errorf("this command takes exactly one argument")
	}
	// TODO: implement this
	return nil
}

func (c BasicCommand) Store(port *goserial.Port, params ...interface{}) error {
	if len(params) != 0 {
		return fmt.Errorf("this command takes no arguments")
	}
	port.TX <- REQ_PERSISTANT + c.code
	t := time.NewTimer(respTimeout)
	var resp string
	select {
	case resp = <-port.RX:
	case <-t.C:
		return ErrTimeout
	}
	switch resp {
	case RESP_OK:
		return nil
	case RESP_UNKNOWN_CMD:
		return ErrUnknownCmd
	case RESP_INVALID_ARG:
		return ErrUnknownCmd
	case RESP_PARTIAL:
		return ErrUnknownCmd
	}
	return ErrInvalidResp
}

func (c BasicCommand) AssignStore(port *goserial.Port, params ...interface{}) error {
	if len(params) != 1 {
		return fmt.Errorf("this command takes exactly one argument")
	}
	// TODO: implement this
	return nil
}

type ParamCommand struct {
	code        string
	description string
	rangeMin    uint
	rangeMax    uint
	exec_time   time.Duration
	get         bool
	assign      bool
	store       bool
	assignStore bool
}

func (c ParamCommand) AllowedMethods() (bool, bool, bool, bool) {
	return c.get, c.assign, c.store, c.assignStore
}

func (c ParamCommand) Get(port *goserial.Port, params ...interface{}) (interface{}, error) {
	if len(params) != 1 {
		return nil, fmt.Errorf("this command takes exactly one argument")
	}
	// TODO: implement this
	return nil, nil
}

func (c ParamCommand) Assign(port *goserial.Port, params ...interface{}) error {
	if len(params) != 2 {
		return fmt.Errorf("this command takes exactly two arguments")
	}
	// TODO: implement this
	return nil
}

func (c ParamCommand) Store(port *goserial.Port, params ...interface{}) error {
	if len(params) != 1 {
		return fmt.Errorf("this command takes exactly one argument")
	}
	// TODO: implement this
	return nil
}

func (c ParamCommand) AssignStore(port *goserial.Port, params ...interface{}) error {
	if len(params) != 2 {
		return fmt.Errorf("this command takes exactly two argument")
	}
	// TODO: implement this
	return nil
}

type Param2Command struct {
	code        string
	description string
	rangeMin    uint
	rangeMax    uint
	exec_time   time.Duration
	get         bool
	assign      bool
	store       bool
	assignStore bool
}

func (c Param2Command) AllowedMethods() (bool, bool, bool, bool) {
	return c.get, c.assign, c.store, c.assignStore
}
func (c Param2Command) Get(port *goserial.Port, params ...interface{}) (interface{}, error) {
	if len(params) != 1 {
		return nil, fmt.Errorf("this command takes exactly one argument")
	}
	// TODO: implement this
	return nil, nil
}

func (c Param2Command) Assign(port *goserial.Port, params ...interface{}) error {
	if len(params) != 2 {
		return fmt.Errorf("this command takes exactly two arguments")
	}
	// TODO: implement this
	return nil
}

func (c Param2Command) Store(port *goserial.Port, params ...interface{}) error {
	if len(params) != 1 {
		return fmt.Errorf("this command takes exactly one argument")
	}
	// TODO: implement this
	return nil
}

func (c Param2Command) AssignStore(port *goserial.Port, params ...interface{}) error {
	if len(params) != 2 {
		return fmt.Errorf("this command takes exactly two argument")
	}
	// TODO: implement this
	return nil
}

var (
	// Basic:
	CMD_AF       = BasicCommand{"AF", "Alternative Frequencies", 0, true, true, true, true}
	CMD_AFCH     = BasicCommand{"AFCH", "Alternative Frequencies Channels", 0, true, true, true, false}
	CMD_DI       = BasicCommand{"DI", "Decoder Identification", 0, true, true, true, false}
	CMD_DPS1     = BasicCommand{"DPS1", "Dynamic PS1", 400 * time.Millisecond, true, true, true, false}
	CMD_DPS1ENQ  = BasicCommand{"DPS1ENQ", "Dynamic PS1 Enqueue", 400 * time.Millisecond, false, true, false, false}
	CMD_DPS2     = BasicCommand{"DPS2", "Dynamic PS2", 400 * time.Millisecond, true, true, true, false}
	CMD_DPS1MOD  = BasicCommand{"DPS1MOD", "Dynamic PS1 Mode", 400 * time.Millisecond, true, true, true, false}
	CMD_DPS2MOD  = BasicCommand{"DPS2MOD", "Dynamic PS2 Mode", 400 * time.Millisecond, true, true, true, false}
	CMD_DPS1REP  = BasicCommand{"DPS1REP", "Dynamic PS2 Number of Repeating", 400 * time.Millisecond, true, true, true, false}
	CMD_DPS2REP  = BasicCommand{"DPS2REP", "Dynamic PS2 Number of Repeating", 400 * time.Millisecond, true, true, true, false}
	CMD_DTTMOUT  = BasicCommand{"DTTMOUT", "Default Text Timeout", 0, true, true, true, false}
	CMD_EQTEXT1  = BasicCommand{"EQTEXT1", "Equal Text 1", 0, true, true, true, false}
	CMD_LABPER   = BasicCommand{"LABPER", "Label Period", 0, true, true, true, false}
	CMD_MS       = BasicCommand{"MS", "Music/Speech", 0, true, true, true, false}
	CMD_PI       = BasicCommand{"PI", "Program Identifaction", 0, true, true, true, false}
	CMD_PS       = BasicCommand{"PS", "Program Service name", 400 * time.Millisecond, true, true, true, false}
	CMD_PTY      = BasicCommand{"PTY", "Program Type number", 0, true, true, true, false}
	CMD_PTYN     = BasicCommand{"PTYN", "Program Type Name", 0, true, true, true, false}
	CMD_PTYNEN   = BasicCommand{"PTYNEN", "PTYN Enable", 0, true, true, true, false}
	CMD_RT1      = BasicCommand{"RT1", "Radiotext 1", 0, true, true, true, false}
	CMD_RT1EN    = BasicCommand{"RT1EN", "RT1 Enable", 0, true, true, true, false}
	CMD_RT2      = BasicCommand{"RT2", "Radiotext 2", 0, true, true, true, false}
	CMD_RT2EN    = BasicCommand{"RT2EN", "RT2 Enable", 0, true, true, true, false}
	CMD_RTPER    = BasicCommand{"RTPER", "Radiotext Switching Period", 0, true, true, true, false}
	CMD_RSTDPS   = BasicCommand{"RSTDPS", "Rerstart Dynamic PS", 0, true, true, true, false}
	CMD_SCRLSPD  = BasicCommand{"SCRLSPD", "Scrolling PS Speed", 0, true, true, true, false}
	CMD_SPSPER   = BasicCommand{"SPSPER", "Static PS Period", 0, true, true, true, false}
	CMD_TA       = BasicCommand{"TA", "Traffic Announcement", 0, true, true, true, false}
	CMD_TATMOUT  = BasicCommand{"TATMOUT", "TA TImeout", 0, true, true, true, false}
	CMD_TP       = BasicCommand{"TP", "Traffic Program", 0, true, true, true, false}
	CMD_TPS      = BasicCommand{"TPS", "Traffic PS", 400 * time.Millisecond, true, true, true, false}
	CMD_INIT     = BasicCommand{"INIT", "Initialization", 0, true, false, false, false}
	CMD_STOREALL = BasicCommand{"ALL", "Store All", 0, false, false, true, false}
	CMD_HELP     = BasicCommand{"HELP", "Help", 0, true, false, false, false}

	// EON:
	CMD_EON_AF   = ParamCommand{"EONxAF", "EON x Frequencies", 1, 4, 0, true, true, false, false}
	CMD_EON_AFCH = ParamCommand{"EONxAFCH", "EON x Frequency channels", 1, 4, 0, true, true, false, false}
	CMD_EON_EN   = ParamCommand{"EONxEN", "EON x Enable", 1, 4, 0, true, true, false, false}
	CMD_EON_PI   = ParamCommand{"EONxPI", "EON x Program Identification", 1, 4, 0, true, true, false, false}
	CMD_EON_PIN  = ParamCommand{"EONxPIN", "EON x Program Item Number", 1, 4, 0, true, true, false, false}
	CMD_EON_PS   = ParamCommand{"EONxPS", "EON x Program Service name", 1, 4, 0, true, true, false, false}
	CMD_EON_PTY  = ParamCommand{"EONxPTY", "EON x Program Type number", 1, 4, 0, true, true, false, false}
	CMD_EON_TA   = ParamCommand{"EONxTA", "EON x Traffic Announcement", 1, 4, 0, true, true, false, false}
	CMD_EON_TP   = ParamCommand{"EONxTA", "EON x Traffic Program", 1, 4, 0, true, true, false, false}
	CMD_EON      = ParamCommand{"EON", "Store all EON data into EEPROM", 1, 4, 50 * time.Millisecond, false, false, true, false}

	//Messages:
	CMD_MSG     = Param2Command{"MSGxx", "Text message", 1, 99, 0, true, false, false, true}
	CMD_MSG_D   = Param2Command{"MSGxxD", "Message Destination", 1, 99, 0, true, false, false, true}
	CMD_MSGLIST = BasicCommand{"MSGLIST", "List of Messages", 0, true, false, false, false}
	CMD_DPS2MSG = BasicCommand{"DPS2MSG", "Dynamic PS 2 Message Number", 0, true, true, true, false}
	CMD_RT2MSG  = BasicCommand{"RT2MSG", "Radiotext 2 Message NUmber", 0, true, true, true, false}

	//Scheduling:
	CMD_SLIST = BasicCommand{"SLIST", "List of Scheduling Items", 0, true, false, false, false}
	CMD_S_C   = Param2Command{"SxxC", "Scheduling Item Command", 1, 48, 0, true, false, false, true}
	CMD_S_D   = Param2Command{"SxxD", "Scheduling Item Days", 1, 48, 0, true, false, false, true}
	CMD_S_P   = Param2Command{"SxxP", "Scheduling Item PTY", 1, 48, 0, true, false, false, true}
	CMD_S_T   = Param2Command{"SxxT", "Scheduling Item Times", 1, 48, 0, true, false, false, true}
	CMD_SEN   = BasicCommand{"SEN", "Scheduling Enable", 0, true, true, true, false}

	// System:
	CMD_COMSPD  = BasicCommand{"COMSPD", "Com Port Speed", 0, true, true, true, false}
	CMD_CT      = BasicCommand{"CT", "CLock Time and Date", 0, true, true, true, false}
	CMD_DATE    = BasicCommand{"Date", "Date", 0, true, true, true, false}
	CMD_ECHO    = BasicCommand{"ECHO", "Terminal Echo", 0, true, true, true, false}
	CMD_EXTSYNC = BasicCommand{"EXTSYNC", "External Pilot Synchronisation", 0, true, true, true, false}
	CMD_LEVEL   = BasicCommand{"LEVEL", "RDS Signal Level", 0, true, true, true, false}
	CMD_LTO     = BasicCommand{"LTO", "Local Time Offset", 0, true, true, true, false}
	CMD_MJD     = BasicCommand{"MJD", "Modified Julian Day", 0, true, true, true, false}
	CMD_PHASE   = BasicCommand{"PHASE", "RDS Signal Phase", 0, true, true, true, false}
	CMD_PILOT   = BasicCommand{"PILOT", "Pilot Tone Present", 0, true, false, false, false}
	CMD_RDSGEN  = BasicCommand{"RDSGEN", "RDS Generator", 0, true, true, true, false}
	CMD_RESET   = BasicCommand{"RESET", "Reset", 0, true, false, false, false}
	CMD_SPEED   = BasicCommand{"SPEED", "Com Port Speed", 0, true, true, true, false}
	CMD_STATUS  = BasicCommand{"STATUS", "RDS Encoder Status", 0, true, false, false, false}
	CMD_TIME    = BasicCommand{"TIME", "Time", 0, true, true, true, false}
	CMD_VER     = BasicCommand{"VER", "Firmware Version", 0, true, false, false, false}

	// Advanved:
	CMD_ADR     = BasicCommand{"ADR", "Encoder Address List", 0, true, false, false, true}
	CMD_CC      = BasicCommand{"CC", "Conditional Command", 0, true, false, false, true}
	CMD_DSN     = ParamCommand{"DSNx", "Program x Data Set Number", 1, 2, 0, true, false, false, true}
	CMD_ECC     = BasicCommand{"ECC", "Extended Country Code", 0, true, true, true, false}
	CMD_ECCEN   = BasicCommand{"ECCEN", "ECC and LIC Enable", 0, true, true, true, false}
	CMD_G       = BasicCommand{"G", "Group", 200 * time.Millisecond, false, true, false, false}
	CMD_GRPSEQ  = BasicCommand{"GRPSEQ", "Group Sequeunce", 0, true, true, true, false}
	CMD_LIC     = BasicCommand{"LIC", "Language Identification Code", 0, true, true, true, false}
	CMD_NOHDR   = BasicCommand{"NOHDR", "No Header Communication", 0, true, false, false, true}
	CMD_PIN     = BasicCommand{"PIN", "Program Item Number", 0, true, true, true, false}
	CMD_PINEN   = BasicCommand{"PINEN", "PIN Enable", 0, true, true, true, false}
	CMD_PROGRAM = BasicCommand{"PROGRAM", "Program Set Selection", 0, true, true, true, false}
	CMD_PSN     = ParamCommand{"PSNx", "Program x Service Number", 1, 2, 0, true, false, false, true}
	CMD_PSW     = BasicCommand{"PSW", "PS Window", 0, true, false, false, false}
	CMD_RTP     = BasicCommand{"RTP", "Radiotext Plus Tagging Data", 0, true, true, false, false}
	CMD_RTPRUN  = BasicCommand{"RTPRUN", "Radiotext Plus Running Bit", 0, true, true, false, false}
	CMD_SEL     = BasicCommand{"SEL", "Select Encoder", 0, false, true, false, false}
	CMD_SHORTRT = BasicCommand{"SEL", "Short Radiotext", 0, true, true, true, false}
	CMD_SITE    = BasicCommand{"SITE", "Site Address List", 0, true, false, false, true}
	CMD_UDG1    = BasicCommand{"UDG1", "User Defined Groups 1", 0, true, true, true, false}
	CMD_UDG2    = BasicCommand{"UDG2", "User Defined Groups 2", 0, true, true, true, false}
	CMD_UECP    = BasicCommand{"UECP", "UECP Enable", 0, true, true, true, false}
)
